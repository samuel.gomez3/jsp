/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.109
 * Generated at: 2023-11-10 14:12:27 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"es\">\n");
      out.write("<head>\n");
      out.write("<meta charset=\"UTF-8\">\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("<title>Contorno para Frame</title>\n");
      out.write("<style>\n");
      out.write("body, html {\n");
      out.write("\theight: 100%; /* Se asegura que el contenedor tenga altura total */\n");
      out.write("\tmargin: 0; /* Elimina el margen por defecto */\n");
      out.write("\tdisplay: flex; /* Habilita Flexbox */\n");
      out.write("\tjustify-content: center; /* Centra horizontalmente */\n");
      out.write("\talign-items: center; /* Centra verticalmente */\n");
      out.write("\tbackground-color: #f8edeb; /* Color de fondo para el body */\n");
      out.write("}\n");
      out.write("\n");
      out.write(".frame {\n");
      out.write("\tborder: 3px solid #4a4e69; /* Color de borde */\n");
      out.write("\tpadding: 20px; /* Espacio entre el borde y el contenido */\n");
      out.write("\tborder-radius: 15px; /* Bordes redondeados */\n");
      out.write("\tbox-shadow: 0 0 10px rgba(0, 0, 0, 0.5);\n");
      out.write("\t/* Sombra para dar efecto de profundidad */\n");
      out.write("\twidth: 700px; /* Ancho del frame */\n");
      out.write("\ttext-align: center; /* Alineación del texto */\n");
      out.write("\tbackground-color: #f2e9e4; /* Color de fondo del frame */\n");
      out.write("}\n");
      out.write("\n");
      out.write(".frame:hover {\n");
      out.write("\tborder-color: #22223b; /* Color de borde al pasar el mouse */\n");
      out.write("\tbox-shadow: 0 0 15px rgba(0, 0, 0, 0.7);\n");
      out.write("\t/* Sombra más pronunciada al pasar el mouse */\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\t<p></p>\n");
      out.write("\t<div class=\"frame\">\n");
      out.write("\t\t<iframe src=\"http://localhost:4200/\" width=\"600\" height=\"400\">\n");
      out.write("\t\t\tTu navegador no soporta iframes. </iframe>\n");
      out.write("\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
