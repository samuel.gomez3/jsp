


<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Contorno para Frame</title>
<style>
body, html {
	height: 100%; /* Se asegura que el contenedor tenga altura total */
	margin: 0; /* Elimina el margen por defecto */
	display: flex; /* Habilita Flexbox */
	justify-content: center; /* Centra horizontalmente */
	align-items: center; /* Centra verticalmente */
	background-color: #f8edeb; /* Color de fondo para el body */
}

.frame {
	border: 3px solid #4a4e69; /* Color de borde */
	padding: 20px; /* Espacio entre el borde y el contenido */
	border-radius: 15px; /* Bordes redondeados */
	box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
	/* Sombra para dar efecto de profundidad */
	width: 700px; /* Ancho del frame */
	text-align: center; /* Alineaci�n del texto */
	background-color: #f2e9e4; /* Color de fondo del frame */
}

.frame:hover {
	border-color: #22223b; /* Color de borde al pasar el mouse */
	box-shadow: 0 0 15px rgba(0, 0, 0, 0.7);
	/* Sombra m�s pronunciada al pasar el mouse */
}
</style>
</head>
<body>
	<p></p>
	<div class="frame">
		<iframe src="http://localhost:4200/" width="600" height="400">
			Tu navegador no soporta iframes. </iframe>

	</div>

</body>
</html>






